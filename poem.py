import random


titres = [
    'Répandre des écrits',
    'Propager des recettes',
    'Partager le pain',
    'Distribuer des semances',
    'Récolter à plusieurs',
    'Colporter des connaissances',
    'Disséminer des idées',
    'Bouturer les mots',
    'Multiplier les proportions',
    'Faire proliférer les récits',
    'Sauter par dessus les clôtures',
    'Emprunter des sentiers boueux',
    'Contourner parfois la loi',
    'Lutter contre la brevétisation du vivant',
    'Préserver la bibliodiversité',
    'Résister face à la monoculture',


        
]

verbes = [
    'Lire', 
    'Prêter',
    'Photocopier',
    'Numériser',
    'Déchirer',
    'Glisser',
    'Copier' ,
    'Photographier',
    'Découvrir',
    'Arpenter',
    'Discuter',
    'Transmettre',
    'Mimer',
    'Performer',
    'Échanger',
    'Annoter',
    'Répéter',
    'Dupliquer',
    'Réécrire',
    'Organiser',
    'Emprunter',
    'Rédiger',
    'Échanger',
    'Ajouter', 
    'Laisser fermenter',
    'Diviser',
    'Découper',
    'Dévorer',
    'Chanter',
    'Cuisiner',
    'Laisser travailler',
    'Pétrir',
    'Archiver',
    'Conserver',
    'Abriter',
    'Prendre soin',
    'Humer',
    'Construire',
    'Libérer',
    'Conserver',
    'Garder',
    'Noter',
    'Conserver',
    'Arroser',
    'Espacer',
    'Favoriser',
    'Monter en graine',
    'Préserver',



]

noms = [
    'le récit',
    'les contre-récits',
    'les paroles', 
    'l’objet',
    'les images',
    'le texte',
    'les pages',
    'le livre',
    'la couverture',
    'les mots',
    'des passages',
    'le pdf',
    'des commentaires',
    'des extraits',
    'une bibliothèque', 
    'des livres',
    'des résumés',
    'des mots',
    'des phrases',
    'des écrits',
    'des sons',
    'le sel',
    'la farine',
    'la pâte',
    'le pain',
    'la mie',
    'la recette',
    'le levain',
    'les graines',
    'des sauvegardes',
    'un four à pain',
    'les savoirs',
    'les tentatives ratées et réussies',
    'les variétés de plantes',
    'les licences libres',
    'les salades',
    'des jachères fleuries',

    


]


qualificatifs = [
    'à haute voix',
    'de mains en mains',
    'avec une imprimante',
    'avec un scanner',
    'avec un ciseau',
    'dans une boîte à livres',
    'sur des clés usb',
    'avec son téléphone',
    'à la radio',
    'collectivement',
    'par mail',
    'en pair à pair',
    'dans les marges',
    'sur les murs',
    'dans des boîtes aux lettres',
    'sous les portes',
    'sur les pare-brises',
    'dans les abris bus',
    'dans les écoles',
    'sur les paillassons',
    'en auto-gestion',
    'à la bibliothèque',
    'sur un blog',
    'avec des inconnu·es',
    'de la même manière que le levain',
    '2h45 à 24 degrés',
    'en petit pâtons',
    'avec un couteau denté',
    'du regard',
    'à tue-tête',
    'à plusieurs',   
    'patiemment', 
    'à quatre mains',
    "à l'abri de l'humidité",
    "à la sortie du four",
    'dans la nature',
    'à plat',
    'en activité',
    'dans un carnet',
    'au frais',
    'toute la journée',
    'dans la terre',
    'du jardin',
    'pour les abeilles',            
]


stanza_count = 11
for repeat in range(stanza_count):
    line_count = 11
    for i in range(line_count):
        if i == 0:
            recipe_name = (random.choice((titres)) + " : \n" )
            print(recipe_name)
        else:   
            
            if i == 10:
                
                ten = (f"{i}. " + random.choice((verbes)) + " " + random.choice((noms)) + " " + random.choice((qualificatifs)) +"\n")
                print(ten)
            else:    
                nine = (f"{i}. " + random.choice((verbes)) + " " + random.choice((noms)) + " " + random.choice((qualificatifs)))
                print(nine)
    
